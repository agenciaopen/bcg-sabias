$(document).ready(function () {
    var sliderNav = $('.carousel_featured_redes');

    if(sliderNav.children('.item_rede').length >= 4) {
        var dots_active = true;
    }else{
        var dots_active = false;

    }
    $('.carousel_featured_redes').slick({
        
        dots: dots_active,
        slidesToShow: 4,
        infinite: true,
        slidesToScroll: 1,
        infinite: true,
  
        responsive: [
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }
        ]
    });
});


