<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="<?php bloginfo('name'); ?> - <?php bloginfo('description'); ?>">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <title><?php wp_title(); ?></title>

    <?php wp_head(); ?>

</head>


<body <?php body_class(); ?> id="body">


    <nav class="navbar navbar-expand-xl navbar-default fixed-top" role="navigation" id="nav_main">
        <div class="container navbar__container">
            <div class="navbar__logos col-12 col-md-6">
                <a class="navbar-brand" href="<?php echo home_url(); ?>">
                    <?php if (get_field('logo_1', 'option')) : ?>
                        <img src="<?php the_field('logo_1', 'option'); ?>" class="navbar__logos__andorinha"/>
                    <?php endif ?>
                </a>
                <?php if (get_field('logo_2', 'option')) : ?>
                    <img src="<?php the_field('logo_2', 'option'); ?>" />
                <?php endif ?>
            </div>
            <ul class="d-none d-md-flex col-6">
                <li><a href="#oferta">Empreendimento</a></li>
                <li><a href="#diferenciais">Diferenciais</a></li>
                <li><a href="#video">Vídeo</a></li>
                <li><a href="#planta">Ficha Técnica</a></li>
                <li><a href="#contato">Contato</a></li>
            </ul>
        </div>
    </nav>